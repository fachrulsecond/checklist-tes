<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Checklist extends Model
{
    //
    use SoftDeletes, HasFactory;

    protected $dates = ['deleted_at'];
    protected $table = 'checklist';
    protected $primaryKey = "checklist_id";
    public $timestamps = true;
    protected $fillable = ['checklist_name'];
}
