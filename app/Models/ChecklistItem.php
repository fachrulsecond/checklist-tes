<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ChecklistItem extends Model
{
    //
    use SoftDeletes, HasFactory;

    protected $dates = ['deleted_at'];
    protected $table = 'checklist_item';
    protected $primaryKey = "checklist_item_id";
    public $timestamps = true;
    protected $fillable = ['checklist_item_name'];
}
