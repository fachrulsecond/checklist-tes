<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Users extends Model
{
    //
    use SoftDeletes, HasFactory;

    protected $dates = ['deleted_at'];
    protected $table = 'users';
    protected $primaryKey = "user_id";
    public $timestamps = true;
    protected $fillable = ['username', 'password', 'email'];
}
