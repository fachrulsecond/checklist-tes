<?php

namespace App\Console\Commands;
use App\Http\Controllers\TagvalController;


use Illuminate\Console\Command;

class TagvalSendKlhk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tagval:SendKlhk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bismillah kirim';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new TagvalController();
        $controller->sendAverageLastHourKLHK();

        return 0;
    }
}
