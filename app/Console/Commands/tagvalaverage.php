<?php

namespace App\Console\Commands;

use App\Http\Controllers\ExportController;
use App\Http\Controllers\TagvalController;
use Illuminate\Console\Command;

class TagvalAverage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tagval:average';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new TagvalController();
        $controller->getAverageLastMinute();

        return 0;
    }
}
