<?php

namespace App\Console\Commands;

use App\Models\Plant;
use App\Models\Tagval;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class cronjob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'query:modusmedian';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Query Modus Median';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set("Asia/Jakarta");

        $tabel = new Tagval();

        $tabel->cnlnum = 1;
        $tabel->val = 20;
        $tabel->val = 20;
        $tabel->user_add = 'cronjob';
        $tabel->user_upd = 'cronjob';

        $tabel->save();

        return 0;
    }
}
