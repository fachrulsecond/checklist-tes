<?php

namespace App\Classes;

use App\Models\Checklist;
use Illuminate\Support\Facades\DB;

class ChecklistClass
{
    public static function show($request)
    {
        $response = DB::table('checklist')->whereNull('checklist.deleted_at')
            ->select('checklist.*');

        if ($request->kriteria) {
            # code...
            $response = $response->where(function ($query) use ($request) {
                $query->where('checklist.checklist_name', 'like', '%' . $request->kriteria . '%');
            });
        }

        return $response;
    }

    public static function getData($id)
    {
        $response = Checklist::where('checklist_id', $id);
        return $response;
    }

    public static function save($request)
    {
        if ($request->checklist_id == '' || !$request->checklist_id) {
            $tabel = new Checklist;

            $tabel->user_add = $request->user_id_log;

            $information = 'Data Saved Successfully';
        } else {
            $tabel = Checklist::where('checklist_id', $request->checklist_id)->first();

            $information = 'Data Updated Successfully';
        }

        $tabel->checklist_name   = $request->itemName;

        $tabel->user_upd = $request->user_id_log;

        $tabel->save();

        return $information;
    }

    public static function checkData($request)
    {
        # code...
        $response = DB::table('checklist')->where($request->field, $request->criteria);
        return $response;
    }

    public static function softDelete($id, $request)
    {
        $tabel = Checklist::where('checklist_id', $id)->first();

        $tabel->user_del = $request->user_id_log;
        $tabel->save();

        $tabel->delete();
        $information = 'Data Deleted Successfully';
        return $information;
    }

    public static function showSoftDelete()
    {
        $response = Checklist::onlyTrashed();
        return $response;
    }

    public static function restoreSoftDelete($id)
    {
        $tabel = Checklist::onlyTrashed()->where('checklist_id', $id)->first();

        $tabel->user_del = '';
        $tabel->save();

        $tabel->restore();
        $information = 'Data Restored Successfully';
        return $information;
    }

    public static function restoreSoftDeleteAll()
    {
        $tabel = Checklist::onlyTrashed();

        $tabel->restore();

        Checklist::where('user_del', '!=', null)
            ->update(['user_del' => null]);

        $information = 'All Data Successfully Restored';
        return $information;
    }

    public static function deletePermanent($id)
    {
        // $tabel = Checklist::onlyTrashed()->where('checklist_id', $id);
        // $tabel->forceDelete();
        $deleted = DB::table('checklist')->where('checklist_id', $id)->delete();
        $information = 'Permanently Deleted Data Successfully';
        return $information;
    }
}
