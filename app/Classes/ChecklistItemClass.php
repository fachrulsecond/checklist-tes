<?php

namespace App\Classes;

use App\Models\ChecklistItem;
use Illuminate\Support\Facades\DB;

class ChecklistItemClass
{
    public static function show($request)
    {
        $response = DB::table('checklist_item')->whereNull('checklist_item.deleted_at')
            ->select('checklist_item.*');

        if (isset($request->parent_id)) {
            # code...
            $response = $response->where(function ($query) use ($request) {
                $query->where('checklist_item.checklist_id',  $request->parent_id);
            });
        }

        if (isset($request->checklist_item_id) && $request->checklist_item_id != 0) {
            # code...
            $response = $response->where(function ($query) use ($request) {
                $query->where('checklist_item.checklist_item_id',  $request->checklist_item_id);
            });
        }

        if (isset($request->kriteria)) {
            # code...
            $response = $response->where(function ($query) use ($request) {
                $query->where('checklist_item.checklist_item_name', 'like', '%' . $request->kriteria . '%');
            });
        }

        return $response;
    }

    public static function getData($id)
    {
        $response = ChecklistItem::where('checklist_item_id', $id);
        return $response;
    }

    public static function save($request)
    {
        if ($request->checklist_item_id == '' || !$request->checklist_item_id) {
            $tabel = new ChecklistItem;
            $tabel->user_add = $request->user_id_log;

            $information = 'Data Saved Successfully';
        } else {
            $tabel = ChecklistItem::where('checklist_item_id', $request->checklist_item_id)->first();

            $information = 'Data Updated Successfully';
        }
        
        $tabel->checklist_id   = $request->checklist_id;

        $tabel->checklist_item_name   = $request->itemName;

        $tabel->user_upd = $request->user_id_log;

        $tabel->save();

        return $information;
    }

    public static function updateStatus($request)
    {
        $tabel = ChecklistItem::where('checklist_item_id', $request->checklist_item_id)->first();

        $request->status = $tabel->status == 0 ? 1 : 0;

        $tabel->status   = $request->status;

        $information = 'Data Updated Successfully';

        $tabel->user_upd = $request->user_id_log;

        $tabel->save();

        return $information;
    }

    public static function rename($request)
    {
        $tabel = ChecklistItem::where('checklist_item_id', $request->checklist_item_id)->first();

        $tabel->checklist_item_name   = $request->itemName;

        $information = 'Data Updated Successfully';

        $tabel->user_upd = $request->user_id_log;

        $tabel->save();

        return $information;
    }

    public static function checkData($request)
    {
        # code...
        $response = DB::table('checklist_item')->where($request->field, $request->criteria);
        return $response;
    }

    public static function softDelete($id, $request)
    {
        $tabel = ChecklistItem::where('checklist_item_id', $id)->first();

        $tabel->user_del = $request->user_id_log;
        $tabel->save();

        $tabel->delete();
        $information = 'Data Deleted Successfully';
        return $information;
    }

    public static function showSoftDelete()
    {
        $response = ChecklistItem::onlyTrashed();
        return $response;
    }

    public static function restoreSoftDelete($id)
    {
        $tabel = ChecklistItem::onlyTrashed()->where('checklist_item_id', $id)->first();

        $tabel->user_del = '';
        $tabel->save();

        $tabel->restore();
        $information = 'Data Restored Successfully';
        return $information;
    }

    public static function restoreSoftDeleteAll()
    {
        $tabel = ChecklistItem::onlyTrashed();

        $tabel->restore();

        ChecklistItem::where('user_del', '!=', null)
            ->update(['user_del' => null]);

        $information = 'All Data Successfully Restored';
        return $information;
    }

    public static function deletePermanent($id)
    {
        // $tabel = ChecklistItem::onlyTrashed()->where('checklist_item_id', $id);
        // $tabel->forceDelete();
        $deleted = DB::table('checklist_item')->where('checklist_item_id', $id)->delete();
        $information = 'Permanently Deleted Data Successfully';
        return $information;
    }
}
