<?php

namespace App\Classes;

use App\Models\Users;
use DateTime;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class UserClass
{
    public static function login($request)
    {
        $response = DB::table('users')->whereNull('users.deleted_at')
            ->where('users.username', $request->username)
            ->where('users.password', $request->password)
            ->select('users.*');

        return $response;
    }

    public static function setLastLogin($id)
    {
        $now = new DateTime();
        $tabel = Users::where('user_id', $id)->first();
        $tabel->lastlogin = $now;
        $tabel->save();
        $information = 'Data Saved Successfully';
        return $information;
    }

    public static function show($request)
    {
        $username_add = DB::raw(Utils::getUsername('users', 'user_add', 'username_add'));
        $username_upd = DB::raw(Utils::getUsername('users', 'user_upd', 'username_upd'));

        $response = DB::table('users')->whereNull('users.deleted_at')
            ->select(
                'user_role.user_role_id',
                'm_roles.role_id',
                'm_roles.role_name',
                'users.*',
                $username_add,
                $username_upd
            )
            ->leftjoin('user_role', 'user_role.user_id', '=', 'users.user_id')
            ->leftjoin('m_roles', 'm_roles.role_id', '=', 'user_role.role_id')
            ->where('users.is_sa', '=', null);

        if ($request->kriteria) {
            # code...
            $response = $response->where(function ($query) use ($request) {
                $query->where('users.username', 'like', '%' . $request->kriteria . '%')
                    ->orWhere('users.name', 'like', '%' . $request->kriteria . '%');
            });
        }

        return $response;
    }

    public static function getData($id)
    {
        $username_add = DB::raw(Utils::getUsername('users', 'user_add', 'username_add'));
        $username_upd = DB::raw(Utils::getUsername('users', 'user_upd', 'username_upd'));

        $response = DB::table('users')->whereNull('users.deleted_at')
            ->select(
                'user_role.user_role_id',
                'm_roles.role_id',
                'm_roles.role_name',
                'm_roles.role_name',
                'm_roles.act_open',
                'm_roles.act_review',
                'm_roles.act_approve',
                'm_roles.act_reject',
                'users.*',
                $username_add,
                $username_upd
            )
            ->leftjoin('user_role', 'user_role.user_id', '=', 'users.user_id')
            ->leftjoin('m_roles', 'm_roles.role_id', '=', 'user_role.role_id')
            ->where('users.user_id', $id);
        return $response;
    }

    public static function getRole($id)
    {
        $response = DB::table('users')->whereNull('users.deleted_at')
            ->select(
                'm_roles.role_name',
                'm_roles.act_open',
                'm_roles.act_review',
                'm_roles.act_approve',
                'm_roles.act_reject'
            )
            ->leftjoin('user_role', 'user_role.user_id', '=', 'users.user_id')
            ->leftjoin('m_roles', 'm_roles.role_id', '=', 'user_role.role_id')
            ->where('users.user_id', $id);
        return $response;
    }

    public static function save($request)
    {
        if ($request->user_id == '' || !$request->user_id) {
            $tabel = new Users;
            $tabel->user_add = $request->user_id_log;

            $information = 'Data Saved Successfully';
        } else {
            $tabel = Users::where('user_id', $request->user_id)->first();

            $information = 'Data Updated Successfully';
        }

        $tabel->email   = $request->email;
        $tabel->username = $request->username;
        $tabel->name = $request->username;
        $tabel->password = $request->password;

        $tabel->user_upd = $request->user_id_log;

        $tabel->save();

        return $information;
    }

    public static function checkData($request)
    {
        # code...
        $response = DB::table('users')->where($request->field, $request->criteria);
        return $response;
    }

    public static function softDelete($id, $request)
    {
        $tabel = Users::where('user_id', $id)->first();

        $tabel->user_del = $request->user_id_log;
        $tabel->save();

        $tabel->delete();
        $information = 'Data Deleted Successfully';
        return $information;
    }

    public static function showSoftDelete()
    {
        $response = Users::onlyTrashed();
        return $response;
    }

    public static function restoreSoftDelete($id)
    {
        $tabel = Users::onlyTrashed()->where('user_id', $id)->first();

        $tabel->user_del = '';
        $tabel->save();

        $tabel->restore();
        $information = 'Data Restored Successfully';
        return $information;
    }

    public static function restoreSoftDeleteAll()
    {
        $tabel = Users::onlyTrashed();

        $tabel->restore();

        Users::where('user_del', '!=', null)
            ->update(['user_del' => null]);

        $information = 'All Data Successfully Restored';
        return $information;
    }

    public static function deletePermanent($id)
    {
        // $tabel = Users::onlyTrashed()->where('user_id', $id);
        // $tabel->forceDelete();
        $deleted = DB::table('users')->where('user_id', $id)->delete();
        $information = 'Permanently Deleted Data Successfully';
        return $information;
    }

    public static function savePhoto($request){
        $tabel = Users::where('user_id', $request->user_id)->first();
        $tabel->path = $request->path;
        $tabel->filename = $request->file_name;

        $tabel->user_upd = $request->user_id_log;

        $tabel->save();

        $information['path'] = $request->path;
        $information['filename'] = $request->file_name;

        return $information;

    }
}
