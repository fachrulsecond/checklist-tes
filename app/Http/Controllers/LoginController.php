<?php

namespace App\Http\Controllers;

use App\Classes\Response;
use Illuminate\Http\Request;

use Firebase\JWT\JWT;

use App\Classes\UserClass;
use Illuminate\Support\Facades\Log;

class LoginController extends RouterController
{
    public function register(Request $request)
    {
        try {
            //code...
            $response = UserClass::save($request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function cekAuth($request)
    {
        try {
            //code...
            $request->username_log = $request->username;
            $response = UserClass::login($request)->get();
            if (count($response) > 0) {
                # code...
                $set_session = $response[0];
                $data_token = [
                    'user_id' => $set_session->user_id,
                    'name' => $set_session->name,
                    'username' => $set_session->username
                ];
                $data_session = [
                    // 'id_user' => $set_session->id,
                    'user_id' => $set_session->user_id,
                    'username' => $set_session->username,
                    'success' => true,
                    'logged' => true,
                    'token' => $this->generateToken($data_token),
                ];
                $response = UserClass::setLastLogin($set_session->user_id);
                $request->user_id_log = $set_session->user_id;
                $request->fullname_log = $set_session->name;
            } else {
                $data_session = [
                    'logged' => false,
                    'success' => false,
                    'message' => 'Invalid Username Or Password'
                ];
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return $data_session;
    }

    public function generateToken($data)
    {
        try {
            //code...
            $payload = [
                'iss' => 'Organization', // Issuer of the token, Organization / Product
                'sub' => 'subject', // Subject of the token
                'iat' => time(), // Time when JWT was issued.
                'data' => $data,
                'exp' => time() + 60 * 60 * 12 // Expiration time (12 jam)
                // 'exp' => time() + 5 // Expiration time
            ];

            $jwt = JWT::encode($payload,  env('JWT_SECRET_KEY'), 'HS256');
            // $decoded = JWT::decode($jwt, new Key(env('JWT_SECRET_KEY'), 'HS256'));
            // var_dump($decoded);
        } catch (\Throwable $th) {
            throw $th;
        }

        return $jwt;
    }

    public function login(Request $request)
    {
        try {
            //code...
            $response = $this->cekAuth($request);
            Log::info('Request : '.$request);
        } catch (\Throwable $th) {
            throw $th;
        }
        Log::info($response);
        echo json_encode($response);
    }

    public function auth($data)
    {
        $response = [
            'logged' => false,
            'success' => false,
            "Message" => $data
        ];
        echo json_encode($response);
    }
}
