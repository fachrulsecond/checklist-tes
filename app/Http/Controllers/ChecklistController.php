<?php

namespace App\Http\Controllers;

use App\Classes\ChecklistClass;
use App\Classes\ChecklistItemClass;
use Illuminate\Http\Request;
use App\Classes\Response;
use Exception;

class ChecklistController extends RouterController
{
    public function show(Request $request)
    {
        try {
            //code...
            $response = ChecklistClass::show($request);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function save(Request $request)
    {
        try {
            //code...
            $response = ChecklistClass::save($request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function update(Request $request)
    {
        try {
            //code...
            $response = ChecklistClass::save($request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function getData($id)
    {
        try {
            //code...
            $response = ChecklistClass::getData($id);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function checkData(Request $request)
    {
        # code...
        try {
            //code...
            $information['field'] = $request->field;
            $information['criteria'] = $request->criteria;
            $response = ChecklistClass::checkData($request);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function softDelete($id, Request $request)
    {
        try {
            //code...
            $request->parent_id = $id;
            $response = ChecklistItemClass::show($request)->get();
            if (count($response)) {
                # code...
                throw new Exception("Checklist is being used");
            }

            $response = ChecklistClass::softDelete($id, $request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function showSoftDelete()
    {
        try {
            //code...
            $response = ChecklistClass::showSoftDelete();
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function restoreSoftDelete($id)
    {
        try {
            //code...
            $response = ChecklistClass::restoreSoftDelete($id);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function restoreSoftDeleteAll()
    {
        try {
            //code...
            $response = ChecklistClass::restoreSoftDeleteAll();
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function deletePermanent($id)
    {
        try {
            //code...
            $response = ChecklistClass::deletePermanent($id);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }
}
