<?php

namespace App\Http\Controllers;

use App\Classes\ChecklistClass;
use App\Classes\ChecklistItemClass;
use Illuminate\Http\Request;
use App\Classes\Response;
use Exception;

class ChecklistItemController extends RouterController
{
    public function show(Request $request)
    {
        try {
            //code...
            $response = ChecklistItemClass::show($request);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function showByParent($checklistId)
    {
        try {
            //code...
            $request = (object) [
                'parent_id' => $checklistId
            ];
            $response = ChecklistItemClass::show($request);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function showByParentAndItemId($checklistId, $checklistItemId)
    {
        try {
            //code...
            $request = (object) [
                'parent_id' => $checklistId,
                'checklist_item_id' => $checklistItemId
            ];
            $response = ChecklistItemClass::show($request);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function save($checklistId, Request $request)
    {
        try {
            //code...
            $request->checklist_id = $checklistId;
            $response = ChecklistItemClass::save($request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function updateStatus($checklistId, $checklistItemId, Request $request)
    {
        try {
            //code...
            $request->checklist_id = $checklistId;
            $request->checklist_item_id = $checklistItemId;
            $response = ChecklistItemClass::updateStatus($request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function rename($checklistId, $checklistItemId, Request $request)
    {
        try {
            //code...
            $request->checklist_id = $checklistId;
            $request->checklist_item_id = $checklistItemId;
            $response = ChecklistItemClass::rename($request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function getData($id)
    {
        try {
            //code...
            $response = ChecklistItemClass::getData($id);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function checkData(Request $request)
    {
        # code...
        try {
            //code...
            $information['field'] = $request->field;
            $information['criteria'] = $request->criteria;
            $response = ChecklistItemClass::checkData($request);
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function softDelete($id, Request $request)
    {
        try {
            //code...
            $request->parent_id = $id;
            // $response = ChecklistItemClass::show($request)->get();
            // if (count($response)) {
            //     # code...
            //     throw new Exception("Checklist is being used");
            // }

            $response = ChecklistItemClass::softDelete($id, $request);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function showSoftDelete()
    {
        try {
            //code...
            $response = ChecklistItemClass::showSoftDelete();
            $information = Response::set('OK', $response->get());
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function restoreSoftDelete($id)
    {
        try {
            //code...
            $response = ChecklistItemClass::restoreSoftDelete($id);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function restoreSoftDeleteAll()
    {
        try {
            //code...
            $response = ChecklistItemClass::restoreSoftDeleteAll();
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }

    public function deletePermanent($id)
    {
        try {
            //code...
            $response = ChecklistItemClass::deletePermanent($id);
            $information = Response::set($response);
        } catch (\Throwable $th) {
            //throw $th;
            $information = Response::setError($th);
        }

        echo json_encode($information);
    }
}
