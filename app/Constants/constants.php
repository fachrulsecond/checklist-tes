<?php

/* STATUS CODE */
define('STATUS_CODE_OK', 200);
define('STATUS_CODE_BAD_REQUEST', 400);
define('STATUS_CODE_UNAUTHORIZED', 401);
define('STATUS_CODE_NOT_FOUND', 404);
define('STATUS_CODE_INTERNAL_SERVER_ERROR', 500);

/* BULAN */
define('BULAN', [1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
/* HARI */
define('HARI', [1 => "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu"]);
/* DATE */
define('DATE_NOW', 'now');
/* DATE FORMAT */
define('DATE_FORMAT_YMDHIS', 'Y-m-d H:i:s');
define('DATE_FORMAT_YMD', 'Y-m-d');
define('DATE_FORMAT_HIS', 'H:i:s');
define('DATE_FORMAT_YEAR', 'Y');
define('DATE_FORMAT_MONTH', 'n');
define('DATE_FORMAT_MONTH_ZERO', 'm');
define('DATE_FORMAT_DAY', 'j');
define('DATE_FORMAT_DAY_ZERO', 'd');
define('DATE_FORMAT_DAY_WEEK', 'N');
define('DATE_FORMAT_HOUR', 'G');
define('DATE_FORMAT_HOUR_ZERO', 'H');
define('DATE_FORMAT_MINUTE_ZERO', 'i');
define('DATE_FORMAT_SECOND_ZERO', 's');
