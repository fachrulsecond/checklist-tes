PGDMP     9    -                {         	   db_ceklis    13.3    13.3 *    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    204318 	   db_ceklis    DATABASE     m   CREATE DATABASE db_ceklis WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United States.1252';
    DROP DATABASE db_ceklis;
                postgres    false            �            1259    204360 	   checklist    TABLE     u  CREATE TABLE public.checklist (
    checklist_id integer NOT NULL,
    checklist_name character varying(255) NOT NULL,
    user_add character varying(255),
    user_upd character varying(255),
    user_del character varying(255),
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE public.checklist;
       public         heap    postgres    false            �            1259    204358    checklist_checklist_id_seq    SEQUENCE     �   CREATE SEQUENCE public.checklist_checklist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.checklist_checklist_id_seq;
       public          postgres    false    207            �           0    0    checklist_checklist_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.checklist_checklist_id_seq OWNED BY public.checklist.checklist_id;
          public          postgres    false    206            �            1259    204349    checklist_item    TABLE     �  CREATE TABLE public.checklist_item (
    checklist_item_id integer NOT NULL,
    checklist_id integer NOT NULL,
    checklist_item_name character varying(255) NOT NULL,
    user_add character varying(255),
    user_upd character varying(255),
    user_del character varying(255),
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    status integer
);
 "   DROP TABLE public.checklist_item;
       public         heap    postgres    false            �            1259    204347 $   checklist_item_checklist_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.checklist_item_checklist_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.checklist_item_checklist_item_id_seq;
       public          postgres    false    205            �           0    0 $   checklist_item_checklist_item_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.checklist_item_checklist_item_id_seq OWNED BY public.checklist_item.checklist_item_id;
          public          postgres    false    204            �            1259    204327 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         heap    postgres    false            �            1259    204325    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public          postgres    false    201            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
          public          postgres    false    200            �            1259    204335    personal_access_tokens    TABLE     �  CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 *   DROP TABLE public.personal_access_tokens;
       public         heap    postgres    false            �            1259    204333    personal_access_tokens_id_seq    SEQUENCE     �   CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.personal_access_tokens_id_seq;
       public          postgres    false    203            �           0    0    personal_access_tokens_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;
          public          postgres    false    202            �            1259    204371    users    TABLE       CREATE TABLE public.users (
    user_id integer NOT NULL,
    name character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(255),
    lastlogin timestamp(0) without time zone,
    user_add character varying(255),
    user_upd character varying(255),
    user_del character varying(255),
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    204369    users_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.users_user_id_seq;
       public          postgres    false    209            �           0    0    users_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;
          public          postgres    false    208            A           2604    204363    checklist checklist_id    DEFAULT     �   ALTER TABLE ONLY public.checklist ALTER COLUMN checklist_id SET DEFAULT nextval('public.checklist_checklist_id_seq'::regclass);
 E   ALTER TABLE public.checklist ALTER COLUMN checklist_id DROP DEFAULT;
       public          postgres    false    206    207    207            @           2604    204352     checklist_item checklist_item_id    DEFAULT     �   ALTER TABLE ONLY public.checklist_item ALTER COLUMN checklist_item_id SET DEFAULT nextval('public.checklist_item_checklist_item_id_seq'::regclass);
 O   ALTER TABLE public.checklist_item ALTER COLUMN checklist_item_id DROP DEFAULT;
       public          postgres    false    204    205    205            >           2604    204330    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    200    201    201            ?           2604    204338    personal_access_tokens id    DEFAULT     �   ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);
 H   ALTER TABLE public.personal_access_tokens ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203            B           2604    204374    users user_id    DEFAULT     n   ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);
 <   ALTER TABLE public.users ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    208    209    209            �          0    204360 	   checklist 
   TABLE DATA           �   COPY public.checklist (checklist_id, checklist_name, user_add, user_upd, user_del, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    207   �5       �          0    204349    checklist_item 
   TABLE DATA           �   COPY public.checklist_item (checklist_item_id, checklist_id, checklist_item_name, user_add, user_upd, user_del, deleted_at, created_at, updated_at, status) FROM stdin;
    public          postgres    false    205   �5       �          0    204327 
   migrations 
   TABLE DATA           :   COPY public.migrations (id, migration, batch) FROM stdin;
    public          postgres    false    201   +6       �          0    204335    personal_access_tokens 
   TABLE DATA           �   COPY public.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, created_at, updated_at) FROM stdin;
    public          postgres    false    203   �6       �          0    204371    users 
   TABLE DATA           �   COPY public.users (user_id, name, username, password, email, lastlogin, user_add, user_upd, user_del, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    209   �6       �           0    0    checklist_checklist_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.checklist_checklist_id_seq', 2, true);
          public          postgres    false    206            �           0    0 $   checklist_item_checklist_item_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.checklist_item_checklist_item_id_seq', 2, true);
          public          postgres    false    204            �           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 4, true);
          public          postgres    false    200            �           0    0    personal_access_tokens_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 1, false);
          public          postgres    false    202            �           0    0    users_user_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.users_user_id_seq', 4, true);
          public          postgres    false    208            K           2606    204357 "   checklist_item checklist_item_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_pkey PRIMARY KEY (checklist_item_id);
 L   ALTER TABLE ONLY public.checklist_item DROP CONSTRAINT checklist_item_pkey;
       public            postgres    false    205            M           2606    204368    checklist checklist_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_pkey PRIMARY KEY (checklist_id);
 B   ALTER TABLE ONLY public.checklist DROP CONSTRAINT checklist_pkey;
       public            postgres    false    207            D           2606    204332    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public            postgres    false    201            F           2606    204343 2   personal_access_tokens personal_access_tokens_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.personal_access_tokens DROP CONSTRAINT personal_access_tokens_pkey;
       public            postgres    false    203            H           2606    204346 :   personal_access_tokens personal_access_tokens_token_unique 
   CONSTRAINT     v   ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);
 d   ALTER TABLE ONLY public.personal_access_tokens DROP CONSTRAINT personal_access_tokens_token_unique;
       public            postgres    false    203            O           2606    204379    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    209            Q           2606    204381    users users_username_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_unique UNIQUE (username);
 E   ALTER TABLE ONLY public.users DROP CONSTRAINT users_username_unique;
       public            postgres    false    209            I           1259    204344 8   personal_access_tokens_tokenable_type_tokenable_id_index    INDEX     �   CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);
 L   DROP INDEX public.personal_access_tokens_tokenable_type_tokenable_id_index;
       public            postgres    false    203    203            �   H   x�3�LJ,*�4�?2202�5��54S04�22�26�&�e֨����	�m������ S��6u\1z\\\ ��o      �   C   x�3�4��,I�U0��?2202�5��54S04�26�21�*f�e�n�S�����V1C�=... ���      �   n   x���1
�@���9�L�S�]c�8�b���S$��A��H�z�s�i��U7[�\�E�u�t����vɌ�8�N�K����TF���oѽW�?ǻ�gw�mCO=q      �      x������ � �      �   X   x�3�LL����$Rs3s���s9���u,t�����-9c�P%\&���٩b0�H��ĢR����0�24�&����� ��.     