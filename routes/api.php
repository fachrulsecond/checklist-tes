<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'App\Http\Controllers\LoginController@login');
Route::post('/register', 'App\Http\Controllers\LoginController@register');

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['authCustom']], function () {
    Route::prefix('checklist')->group(function () {
        Route::get('/checklist', 'App\Http\Controllers\ChecklistController@show');
        Route::post('/checklist', 'App\Http\Controllers\ChecklistController@save');
        Route::delete('/checklist/{id}', 'App\Http\Controllers\ChecklistController@softDelete');
    });

    Route::prefix('checklist-item')->group(function () {
        Route::get('/checklist/{checklistId}/item', 'App\Http\Controllers\ChecklistItemController@showByParent');
        Route::post('/checklist/{checklistId}/item', 'App\Http\Controllers\ChecklistItemController@save');
        Route::get('/checklist/{checklistId}/item/{checklistItemId}', 'App\Http\Controllers\ChecklistItemController@showByParentAndItemId');

        Route::put('/checklist/{checklistId}/item/{checklistItemId}', 'App\Http\Controllers\ChecklistItemController@updateStatus');
        Route::delete('/checklist/{checklistId}/item/{checklistItemId}', 'App\Http\Controllers\ChecklistItemController@softDelete');
        Route::put('/checklist/{checklistId}/item/rename/{checklistItemId}', 'App\Http\Controllers\ChecklistItemController@rename');
    });
});
